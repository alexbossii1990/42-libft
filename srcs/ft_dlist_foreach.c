/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlist_foreach.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/19 14:58:14 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/19 14:59:06 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_dlist.h"
#include <stdlib.h>

void				dlist_foreach(t_dlist *list, t_apl_func afunc)
{
	t_dlist_node	*it;

	it = list->first;
	while (it != NULL)
	{
		afunc(it->item);
		it = it->next;
	}
}

void				dlist_cforeach(t_dlist const *list, t_apl_func afunc)
{
	t_dlist_node const	*it;

	it = list->first;
	while (it != NULL)
	{
		afunc(it->item);
		it = it->next;
	}
}
