/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minmax_float.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/28 05:27:39 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 05:28:12 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

float				ft_min_float(float a, float b)
{
	if (a < b)
		return (a);
	else
		return (b);
}

float				ft_max_float(float a, float b)
{
	if (a > b)
		return (a);
	else
		return (b);
}
