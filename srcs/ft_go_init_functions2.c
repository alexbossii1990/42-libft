/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_go_init_functions2.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 18:20:41 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/30 00:39:44 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_getopt_flag.h>
#include <ft_string.h>

void	ft_go_init_key_value_flag(t_go_flag *flag, char const *arg, size_t i)
{
	char	*equal_pos;

	equal_pos = ft_strchr(arg, '=');
	flag->key = ft_strndup(arg, equal_pos - arg);
	flag->value = ft_strdup(equal_pos + 1);
	(void)i;
}

void	ft_go_init_value_flag(t_go_flag *flag, char const *arg, size_t i)
{
	flag->key = ft_uitoa(i);
	flag->value = ft_strdup(arg);
}
