/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getflag.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/16 16:37:43 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 04:56:14 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_getflag.h"
#include "ft_string.h"
#include <stdlib.h>

void		getflag_init(t_getflag *getflag,
							char const *available_flags,
							t_getflag_err_cb cb)
{
	const size_t	count = ft_strlen(available_flags);
	size_t			i;

	i = 0;
	getflag->fgpairs = (t_getflag_pair *)malloc(sizeof(*getflag->fgpairs)
						* (count));
	while (i < count)
	{
		getflag->fgpairs[i].char_value = available_flags[i];
		getflag->fgpairs[i].enabled = false;
		++i;
	}
	getflag->count = count;
	getflag->error_cb = cb;
	getflag->available_flags = ft_strdup(available_flags);
}

void		getflag_destroy(t_getflag *getflag)
{
	if (getflag->fgpairs)
	{
		free(getflag->fgpairs);
		getflag->fgpairs = NULL;
	}
	if (getflag->available_flags)
	{
		free(getflag->available_flags);
		getflag->available_flags = NULL;
	}
	getflag->count = 0;
}

t_bool		getflag_enable(t_getflag *getflag, char c)
{
	size_t	i;

	i = 0;
	while (i < getflag->count)
	{
		if (getflag->fgpairs[i].char_value == c)
		{
			getflag->fgpairs[i].enabled = true;
			return (true);
		}
		++i;
	}
	return (false);
}

t_bool		getflag_is_enabled(t_getflag const *getflag, char c)
{
	size_t	i;

	i = 0;
	while (i < getflag->count)
	{
		if (getflag->fgpairs[i].char_value == c)
			return (getflag->fgpairs[i].enabled);
		++i;
	}
	return (false);
}

char const	*getflag_available_flags(t_getflag const *getflag)
{
	return (getflag->available_flags);
}
