/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strbuf_replace.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/08 23:31:19 by irabeson          #+#    #+#             */
/*   Updated: 2014/06/01 19:20:51 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_strbuf.h"
#include "ft_string.h"

static void	init_find(t_bool *match, t_ui *i, t_ui *j, t_ui *temp_start)
{
	*i = 0;
	*j = 0;
	*match = false;
	*temp_start = 0;
}

t_bool		strbuf_find(t_strbuf *strbuf, char const *search, t_ui *start)
{
	t_ui	i;
	t_ui	j;
	t_ui	temp_start;
	t_bool	match;

	init_find(&match, &i, &j, &temp_start);
	while (i < strbuf_size(strbuf) && search[j] != '\0')
	{
		if (strbuf_get_char(strbuf, i) == search[j])
		{
			if (match == false)
				temp_start = i;
			match = true;
			++j;
		}
		else
		{
			match = false;
			j = 0;
		}
		++i;
	}
	if (match && start)
		*start = temp_start;
	return (match);
}

void		strbuf_replace_all(t_strbuf *strbuf,
								char const *rep_str,
								char const *new_str)
{
	t_ui	start;

	while (strbuf_find(strbuf, rep_str, &start))
	{
		strbuf_erase(strbuf, start, ft_strlen(rep_str));
		strbuf_insert_str(strbuf, start, new_str);
	}
}
