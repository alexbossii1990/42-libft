/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 18:29:20 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 05:07:50 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

char	*ft_strnstr(char const *source, char const *search, size_t n)
{
	size_t	search_len;
	size_t	source_len;
	size_t	i;

	if (search == NULL)
		return ((char *)source);
	search_len = ft_strlen(search);
	source_len = ft_strlen(source);
	if (search_len > n)
		search_len = n;
	if (source_len < search_len)
		return (NULL);
	if (search_len == 0 || source_len == 0)
		return ((char *)source);
	i = 0;
	while (*source != '\0' && (i + search_len) <= n)
	{
		if (ft_strncmp(source, search, search_len) == 0)
			return ((char *)source);
		source += 1;
		++i;
	}
	return (NULL);
}
