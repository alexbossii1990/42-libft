/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlist_query.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/19 14:57:04 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/19 14:57:53 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_dlist.h"
#include <stdlib.h>

t_bool				dlist_empty(t_dlist const *list)
{
	return (list->first == NULL);
}

t_ui				dlist_count(t_dlist const *list)
{
	t_ui			i;
	t_dlist_node	*it;

	i = 0;
	it = list->first;
	if (it == NULL)
		return (0);
	while (it)
	{
		it = it->next;
		++i;
	}
	return (i);
}
