/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 16:54:13 by irabeson          #+#    #+#             */
/*   Updated: 2013/11/27 12:50:14 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *str, int c, size_t n)
{
	unsigned char	*str_ptr;
	size_t			i;

	str_ptr = (unsigned char *)str;
	if (str)
	{
		i = 0;
		while (i < n)
		{
			if (*str_ptr == (unsigned char)c)
				return ((void *)str_ptr);
			i += 1;
			str_ptr += 1;
		}
	}
	return (NULL);
}
