/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlist.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/19 14:45:38 by irabeson          #+#    #+#             */
/*   Updated: 2014/06/01 07:16:04 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_dlist.h"
#include <stdlib.h>

void				dlist_init(t_dlist *list, t_del_func dfunc)
{
	list->dfunc = dfunc;
	list->first = NULL;
	list->last = NULL;
}

void				dlist_destroy(t_dlist *list)
{
	dlist_clear(list);
}

t_dlist_node		*dlist_insert(t_dlist *list, t_dlist_node *at, void *item)
{
	t_dlist_node	*next;
	t_dlist_node	*prev;
	t_dlist_node	*node;

	node = (t_dlist_node *)malloc(sizeof(*node));
	if (node == NULL)
		return (NULL);
	dlist_node_init(node, item);
	next = at;
	prev = at->prev;
	dlist_node_set_next(node, next);
	dlist_node_set_prev(node, prev);
	if (at == list->first)
		list->first = node;
	if (at == list->last)
		list->last = node;
	return (node);
}
