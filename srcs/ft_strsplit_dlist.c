/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit_dlist.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/21 23:36:13 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/25 19:04:04 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int				next_word(char const *str_it, char const **begin,
									char const **end, char sep)
{
	(*begin) = 0;
	(*end) = 0;
	while (*str_it != '\0' && *str_it == sep)
		str_it = str_it + 1;
	if (*str_it != '\0')
		(*begin) = str_it;
	while (*str_it != '\0' && *str_it != sep)
		str_it = str_it + 1;
	if ((*begin) != '\0')
		(*end) = str_it;
	return (*begin != '\0');
}

static unsigned int		count_word(char const *str, char sep)
{
	unsigned int	word_count;

	word_count = 0;
	while (*str != '\0')
	{
		while (*str == sep)
			str = str + 1;
		if (*str != '\0')
			word_count = word_count + 1;
		while (*str != sep && *str != '\0')
			str = str + 1;
	}
	return (word_count);
}

static char				*extract_str(char const *begin, char const *end)
{
	char	*result;
	int		i;

	if (begin == 0 || end == 0)
		return (NULL);
	i = 0;
	result = malloc(sizeof(*result) * ((end - begin) + 1));
	while (begin != end)
	{
		result[i] = *begin;
		begin = begin + 1;
		i = i + 1;
	}
	result[i] = '\0';
	return (result);
}

unsigned int			ft_strsplit_dlist(char const *str, char sep,
											t_dlist *list)
{
	unsigned int	word_count;
	unsigned int	i;
	char const		*begin;
	char const		*end;

	if (!str)
		return (0);
	word_count = count_word(str, sep);
	i = 0;
	end = str;
	while (i < word_count)
	{
		next_word(end, &begin, &end, sep);
		dlist_push_back(list, (void *)extract_str(begin, end));
		i = i + 1;
	}
	return (word_count);
}
