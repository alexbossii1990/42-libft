/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gnl.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/20 01:18:39 by irabeson          #+#    #+#             */
/*   Updated: 2015/01/23 16:59:05 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_gnl.h"
#include "ft_string.h"
#include <unistd.h>

void			gnl_init(t_gnl *gnl)
{
	strbuf_init(&gnl->buffer, GNL_BUFFER_SIZE);
}

void			gnl_destroy(t_gnl *gnl)
{
	strbuf_destroy(&gnl->buffer);
}

static int		imp_read(t_gnl *gnl, int fd)
{
	static char	buffer[GNL_BUFFER_SIZE];
	ssize_t		readed;

	readed = read(fd, buffer, GNL_BUFFER_SIZE);
	if (readed > 0)
		strbuf_app_strn(&gnl->buffer, buffer, readed);
	return (readed);
}

int				gnl_readline(t_gnl *gnl, int fd, char **line)
{
	t_ui	eol_pos;
	ssize_t	readed;

	*line = NULL;
	readed = imp_read(gnl, fd);
	if (strbuf_find_char(&gnl->buffer, '\n', &eol_pos) == false)
		imp_read(gnl, fd);
	if (strbuf_size(&gnl->buffer) == 0)
		return (GNL_END);
	if (eol_pos == 0)
		*line = ft_strdup("");
	else
		*line = strbuf_getsub(&gnl->buffer, 0, eol_pos);
	strbuf_erase(&gnl->buffer, 0, eol_pos + 1);
	if (readed >= 0)
		return (GNL_CONTINUE);
	return (*line == NULL ? GNL_ERROR : GNL_END);
}
