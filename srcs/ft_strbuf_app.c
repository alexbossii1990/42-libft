/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strbuf_app.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/18 21:38:05 by irabeson          #+#    #+#             */
/*   Updated: 2014/05/18 21:38:08 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_strbuf.h"
#include "ft_string.h"
#include "ft_numeric.h"

void	strbuf_app_char(t_strbuf *strbuf, char c)
{
	if (strbuf->capacity == 0)
		strbuf_reserve(strbuf, 16);
	if (strbuf->size + 1 >= strbuf->capacity)
		strbuf_reserve(strbuf, strbuf->capacity * 2);
	strbuf->buffer[strbuf->size++] = c;
}

void	strbuf_app_str(t_strbuf *strbuf, char const *str)
{
	t_ui const	str_len = ft_strlen(str);
	t_ui		new_cap;

	if (strbuf->capacity == 0)
		strbuf_reserve(strbuf, 16);
	new_cap = strbuf->capacity;
	while (strbuf->size + str_len >= new_cap)
		new_cap *= 2;
	strbuf_reserve(strbuf, new_cap);
	ft_strcpy(strbuf->buffer + strbuf->size, str);
	strbuf->size += str_len;
}

void	strbuf_app_strn(t_strbuf *strbuf, char const *str, t_ui n)
{
	t_ui		new_cap;

	if (strbuf->capacity == 0)
		strbuf_reserve(strbuf, 16);
	new_cap = strbuf->capacity;
	while (strbuf->size + n >= new_cap)
		new_cap *= 2;
	strbuf_reserve(strbuf, new_cap);
	ft_strncpy(strbuf->buffer + strbuf->size, str, n);
	strbuf->size += n;
}
