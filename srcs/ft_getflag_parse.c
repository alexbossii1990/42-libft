/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getflag_parse.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/16 17:08:49 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 04:56:29 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_getflag.h"
#include "ft_string.h"

static t_bool	is_flag(char *arg)
{
	return (arg[0] == '-' && ft_strlen(arg) > 1);
}

static t_bool	process_flag(t_getflag *getflag, char flag)
{
	if (getflag_is_enabled(getflag, flag))
	{
		if (getflag->error_cb)
			getflag->error_cb(getflag, REPEATED_FLAG, flag);
	}
	else if (getflag_enable(getflag, flag) == false)
	{
		if (getflag->error_cb)
		{
			getflag->error_cb(getflag, INVALID_FLAG, flag);
			return (false);
		}
	}
	return (true);
}

int				getflag_parse_args(t_getflag *getflag, int argc, char **argv)
{
	int		i;
	size_t	j;
	char	*arg;

	i = 1;
	while (i < argc)
	{
		arg = argv[i];
		if (ft_strequ(arg, "--"))
			return (i + 1);
		if (is_flag(arg))
		{
			j = 1;
			while (arg[j])
			{
				if (process_flag(getflag, arg[j]) == false)
					return (-1);
				++j;
			}
		}
		else
			break ;
		++i;
	}
	return (i);
}
