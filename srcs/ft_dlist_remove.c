/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlist_remove.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/01 07:16:12 by irabeson          #+#    #+#             */
/*   Updated: 2014/06/01 09:34:07 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_dlist.h"
#include "ft_dlist_node.h"
#include <stdlib.h>

t_dlist_node		*dlist_erase(t_dlist *list, t_dlist_node *node)
{
	t_dlist_node	*next;
	t_dlist_node	*prev;

	if (node == NULL)
		return (NULL);
	next = node->next;
	prev = node->prev;
	if (next)
		next->prev = prev;
	if (prev)
		prev->next = next;
	if (node == list->first)
		list->first = next;
	if (node == list->last)
		list->last = prev;
	dlist_node_destroy(node, list->dfunc);
	free(node);
	return (next);
}

t_dlist_node		*dlist_remove(t_dlist *list, t_dlist_node *node)
{
	t_dlist_node	*next;
	t_dlist_node	*prev;

	if (node == NULL)
		return (NULL);
	next = node->next;
	prev = node->prev;
	if (next)
		next->prev = prev;
	if (prev)
		prev->next = next;
	if (node == list->first)
		list->first = next;
	if (node == list->last)
		list->last = prev;
	dlist_node_destroy(node, NULL);
	free(node);
	return (next);
}

void				dlist_clear(t_dlist *list)
{
	t_dlist_node	*it;
	t_dlist_node	*next;

	it = list->first;
	next = NULL;
	while (it)
	{
		next = it->next;
		dlist_node_destroy(it, list->dfunc);
		free(it);
		it = next;
	}
	list->first = NULL;
	list->last = NULL;
}
