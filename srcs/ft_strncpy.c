/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 05:42:15 by irabeson          #+#    #+#             */
/*   Updated: 2013/11/26 05:43:00 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dest, const char *source, size_t n)
{
	size_t	i;

	i = 0;
	while (source[i] != '\0' && i < n)
	{
		dest[i] = source[i];
		i += 1;
	}
	while (i < n)
	{
		dest[i] = '\0';
		i += 1;
	}
	return (dest);
}
