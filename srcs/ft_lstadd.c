/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/27 14:50:18 by irabeson          #+#    #+#             */
/*   Updated: 2013/11/27 20:18:40 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd(t_list **alst, t_list *new_lst)
{
	if (alst && new_lst)
	{
		if (*alst)
		{
			new_lst->next = *alst;
			*alst = new_lst;
		}
		else
			*alst = new_lst;
	}
}
