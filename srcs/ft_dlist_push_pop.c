/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlist_push_pop.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/19 14:54:10 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/19 14:56:38 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_dlist.h"
#include <stdlib.h>

t_dlist_node		*dlist_push_front(t_dlist *list, void *item)
{
	t_dlist_node	*node;

	node = (t_dlist_node *)malloc(sizeof(*node));
	if (node == NULL)
		return (NULL);
	dlist_node_init(node, item);
	dlist_node_set_next(node, list->first);
	list->first = node;
	if (list->last == NULL)
		list->last = node;
	return (node);
}

t_dlist_node		*dlist_push_back(t_dlist *list, void *item)
{
	t_dlist_node	*node;

	node = (t_dlist_node *)malloc(sizeof(*node));
	if (node == NULL)
		return (NULL);
	dlist_node_init(node, item);
	dlist_node_set_prev(node, list->last);
	list->last = node;
	if (list->first == NULL)
		list->first = node;
	return (node);
}

void				dlist_pop_front(t_dlist *list)
{
	t_dlist_node	*new_first;

	if (list->first == NULL)
		return ;
	new_first = list->first->next;
	if (new_first)
		new_first->prev = NULL;
	if (list->first == list->last)
		list->last = new_first;
	list->first = new_first;
}

void				dlist_pop_back(t_dlist *list)
{
	t_dlist_node	*new_last;

	if (list->last == NULL)
		return ;
	new_last = list->last->prev;
	if (new_last)
		new_last->next = NULL;
	if (list->first == list->last)
		list->first = new_last;
	list->last = new_last;
}
