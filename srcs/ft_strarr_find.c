/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strarr_find.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/26 17:08:54 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 06:58:33 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_strarr.h"
#include "ft_string.h"

t_bool	strarr_find(char **array, char const *str, t_ui *index)
{
	t_ui	i;

	i = 0;
	if (array == NULL)
		return (false);
	while (array[i])
	{
		if (ft_strequ(array[i], str))
		{
			if (index)
				*index = i;
			return (true);
		}
		++i;
	}
	return (false);
}
