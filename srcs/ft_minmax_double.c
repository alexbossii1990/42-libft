/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minmax_double.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/13 01:00:23 by irabeson          #+#    #+#             */
/*   Updated: 2014/05/13 01:01:15 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

double				ft_min_double(double a, double b)
{
	if (a < b)
		return (a);
	else
		return (b);
}

double				ft_max_double(double a, double b)
{
	if (a > b)
		return (a);
	else
		return (b);
}
