/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_abs_ll.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/28 05:46:45 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 05:46:55 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

long long	ft_abs_ll(long long value)
{
	if (value < 0)
		return (-value);
	else
		return (value);
}
