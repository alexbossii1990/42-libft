/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getopt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@tudent.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/12 01:54:53 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/30 00:24:10 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_getopt.h>
#include <ft_getopt_flag.h>
#include <ft_string.h>
#include <stdlib.h>

static int	go_flag_cmp(t_go_flag *left, t_go_flag *right)
{
	return (ft_strcmp(left->key, right->key));
}

static void	go_flag_destroy(void *value)
{
	t_go_flag	*flag;

	flag = (t_go_flag *)value;
	ft_go_flag_destroy(flag);
	free(flag);
}

void		ft_getopt_init_args(t_getopt *getopt, int argc, char **argv)
{
	int			i;

	i = 1;
	ft_getopt_init(getopt);
	while (i < argc)
	{
		ft_getopt_parse(getopt, argv[i], i);
		++i;
	}
}

void		ft_getopt_init(t_getopt *getopt)
{
	btree_init(&getopt->flags, &go_flag_destroy, &go_flag_cmp);
}

void		ft_getopt_destroy(t_getopt *getopt)
{
	btree_destroy(&getopt->flags);
}
