/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strbuf_erase.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/04 15:51:37 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 06:28:31 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_strbuf.h"
#include "ft_memory.h"

t_ui	strbuf_erase(t_strbuf *strbuf, t_ui start, t_ui length)
{
	if (start >= strbuf->size)
		return (0);
	if (start + length >= strbuf->size)
		length = strbuf->size - start;
	if (length == 0)
		return (0);
	ft_memmove(strbuf->buffer + start,
				strbuf->buffer + start + length,
				strbuf->size - (start + length));
	strbuf->size -= length;
	return (length);
}
