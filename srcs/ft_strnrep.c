/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnrep.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/29 17:33:29 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 07:33:25 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"
#include "ft_strbuf.h"

char	*ft_strnrep(char const *origin, t_ui begin, t_ui size, char const *rep)
{
	t_strbuf	buffer;
	char		*str;

	strbuf_init(&buffer, 16);
	if (begin > 0)
		strbuf_app_strn(&buffer, origin, begin);
	strbuf_app_str(&buffer, rep);
	if (ft_strlen(origin + size) > 0)
		strbuf_app_str(&buffer, origin + begin + size);
	str = strbuf_get(&buffer);
	strbuf_destroy(&buffer);
	return (str);
}
