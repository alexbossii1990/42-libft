/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 09:19:26 by irabeson          #+#    #+#             */
/*   Updated: 2013/11/27 12:39:32 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	*ft_memalloc(size_t size)
{
	unsigned char	*mem;
	size_t			i;

	mem = (unsigned char *)malloc(sizeof(*mem) * size);
	if (mem != NULL)
	{
		i = 0;
		while (i < size)
		{
			mem[i] = '\0';
			i += 1;
		}
	}
	return ((void *)mem);
}
