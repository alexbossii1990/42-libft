/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson42@gmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 16:06:49 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/14 20:13:38 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *str, int c)
{
	const char	real_char = (char)c;

	while (*str != real_char)
	{
		if (*str == '\0')
			return (NULL);
		str += 1;
	}
	return ((char *)str);
}
