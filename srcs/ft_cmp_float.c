/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cmp_float.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/28 05:47:40 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 07:43:29 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_numeric.h"
#include <float.h>

t_bool	ft_float_equ(float a, float b)
{
	return (ft_abs_f(a - b) < FLT_EPSILON);
}

t_bool	ft_float_nequ(float a, float b)
{
	return (ft_abs_f(a - b) > FLT_EPSILON);
}

t_bool	ft_float_gequ(float a, float b)
{
	return (ft_abs_f(a - b) < FLT_EPSILON || a > b);
}

t_bool	ft_float_lequ(float a, float b)
{
	return (ft_abs_f(a - b) < FLT_EPSILON || a < b);
}
