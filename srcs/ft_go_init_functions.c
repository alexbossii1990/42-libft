/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_go_init_functions.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@tudent.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/12 02:48:57 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/30 00:39:53 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_getopt_flag.h>
#include <ft_string.h>

void	ft_go_init_dash_flag(t_go_flag *flag, char const *arg, size_t i)
{
	flag->key = ft_strdup(arg + 1);
	flag->value = NULL;
	(void)i;
}

void	ft_go_init_ddash_flag(t_go_flag *flag, char const *arg, size_t i)
{
	flag->key = ft_strdup(arg + 2);
	flag->value = NULL;
	(void)i;
}

void	ft_go_init_dash_value_flag(t_go_flag *flag, char const *arg, size_t i)
{
	char	*equal_pos;

	++arg;
	equal_pos = ft_strchr(arg, '=');
	flag->key = ft_strndup(arg, equal_pos - arg);
	flag->value = ft_strdup(equal_pos + 1);
	(void)i;
}

void	ft_go_init_ddash_value_flag(t_go_flag *flag, char const *arg, size_t i)
{
	char	*equal_pos;

	arg += 2;
	equal_pos = ft_strchr(arg, '=');
	flag->key = ft_strndup(arg, equal_pos - arg);
	flag->value = ft_strdup(equal_pos + 1);
	(void)i;
}
