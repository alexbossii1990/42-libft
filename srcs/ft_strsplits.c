/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplits.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/28 07:06:33 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 07:13:22 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"
#include <stdlib.h>

static int		next_word(char const *str_it, char const **begin,
							char const **end, char const *seps)
{
	(*begin) = 0;
	(*end) = 0;
	while (*str_it != '\0' && ft_strchr(seps, *str_it))
		++str_it;
	if (*str_it != '\0')
		(*begin) = str_it;
	while (*str_it != '\0' && !ft_strchr(seps, *str_it))
		++str_it;
	if ((*begin) != '\0')
		(*end) = str_it;
	return (*begin != '\0');
}

static int		count_word(char const *str, char const *seps)
{
	int	word_count;

	word_count = 0;
	while (*str != '\0')
	{
		while (ft_strchr(seps, *str))
			++str;
		if (*str != '\0')
			++word_count;
		while (!ft_strchr(seps, *str) && *str != '\0')
			++str;
	}
	return (word_count);
}

static char		*extract_str(char const *begin, char const *end)
{
	char	*result;
	int		i;

	if (begin == 0 || end == 0)
		return (NULL);
	i = 0;
	result = malloc(sizeof(*result) * ((end - begin) + 1));
	while (begin != end)
		result[i++] = *begin++;
	result[i] = '\0';
	return (result);
}

char			**ft_strsplits(char const *str, char const *seps)
{
	char		**array_ptr;
	int			word_count;
	int			i;
	char const	*begin;
	char const	*end;

	if (!str)
		return (NULL);
	word_count = count_word(str, seps);
	array_ptr = (char **)malloc(sizeof(*array_ptr) * (word_count + 1));
	i = 0;
	end = str;
	while (i < word_count)
	{
		next_word(end, &begin, &end, seps);
		array_ptr[i++] = extract_str(begin, end);
	}
	array_ptr[i] = '\0';
	return (array_ptr);
}
