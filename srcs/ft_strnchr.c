/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/21 20:57:32 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/21 21:01:14 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnchr(const char *str, int c, size_t n)
{
	const char	real_char = (char)c;
	size_t		i;

	i = 0;
	while (str[i] != '\0' && i < n)
	{
		if (str[i] == real_char)
			return ((char *)str + i);
		++i;
	}
	return (NULL);
}
