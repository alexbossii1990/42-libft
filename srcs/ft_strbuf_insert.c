/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strbuf_insert.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/04 16:22:14 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 06:45:09 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_strbuf.h"
#include "ft_memory.h"
#include "ft_string.h"
#include "ft_numeric.h"

void	strbuf_grow(t_strbuf *strbuf, t_ui to_add)
{
	t_ui	new_cap;

	if (to_add == 0)
		return ;
	new_cap = strbuf->capacity;
	while (strbuf->size + to_add >= new_cap)
		new_cap *= 2;
	strbuf_reserve(strbuf, new_cap);
}

void	strbuf_right(t_strbuf *strbuf, t_ui start, t_ui offset)
{
	if (start >= strbuf->size)
		return ;
	ft_memmove(strbuf->buffer + start + offset,
				strbuf->buffer + start,
				strbuf->size - start);
}

void	strbuf_insert_char(t_strbuf *strbuf, t_ui pos, char c)
{
	if (strbuf->size > 0 && pos < strbuf->size)
	{
		strbuf_right(strbuf, pos, 1);
		strbuf->buffer[pos] = c;
		strbuf->size += 1;
	}
	else
	{
		strbuf_app_char(strbuf, c);
	}
}

void	strbuf_insert_str(t_strbuf *strbuf, t_ui pos, char const *str)
{
	t_ui	str_size;

	if (str == NULL)
		return ;
	str_size = ft_strlen(str);
	if (strbuf->size > 0 && pos < strbuf->size)
	{
		strbuf_right(strbuf, pos, str_size);
		ft_memcpy(strbuf->buffer + pos, str, str_size);
		strbuf->size += str_size;
	}
	else
	{
		strbuf_app_str(strbuf, str);
	}
}

void	strbuf_insert_strn(t_strbuf *strbuf,
							t_ui pos,
							char const *str, t_ui n)
{
	t_ui	str_size;

	if (str == NULL)
		return ;
	str_size = ft_min_uint(ft_strlen(str), n);
	if (strbuf->size > 0 && pos < strbuf->size)
	{
		strbuf_right(strbuf, pos, str_size);
		ft_memcpy(strbuf->buffer + pos, str, str_size);
		strbuf->size = str_size;
	}
	else
	{
		strbuf_app_strn(strbuf, str, n);
	}
}
