/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getopt_parse.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@tudent.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/12 03:50:09 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/30 00:24:14 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_getopt.h>
#include <stdlib.h>

t_go_flag		*ft_getopt_parse(t_getopt *getopt, char const *str, size_t i)
{
	t_go_ftype		ftype;
	t_go_flag		*flag;

	ftype = GOFT_INVALID;
	flag = (t_go_flag *)malloc(sizeof(*flag));
	if (flag)
	{
		ftype = ft_go_flag_init(flag, str, i);
		if (ftype == GOFT_INVALID)
		{
			ft_go_flag_destroy(flag);
			free(flag);
			flag = NULL;
		}
		else
			btree_insert(&getopt->flags, (void *)flag);
	}
	return (flag);
}
