/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson42@gmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 04:07:03 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 05:11:35 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

int		ft_strnequ(char const *str1, char const *str2, size_t n)
{
	if (str1 && str2 && ft_strncmp(str1, str2, n) == 0)
		return (1);
	else
		return (0);
}
