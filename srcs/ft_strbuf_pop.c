/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strbuf_pop.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/04 15:58:49 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 06:30:22 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_strbuf.h"

void	strbuf_pop_front(t_strbuf *strbuf)
{
	strbuf_erase(strbuf, 0, 1);
}

void	strbuf_pop_back(t_strbuf *strbuf)
{
	if (strbuf->size > 0)
		--strbuf->size;
}
