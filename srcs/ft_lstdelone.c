/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/27 14:28:37 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 05:26:46 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_memory.h"

void	ft_lstdelone(t_list **alist, void (*del)(void *, size_t))
{
	t_list	*list;

	if (alist && *alist)
	{
		list = *alist;
		if (del && list->content != NULL && list->content_size > 0)
			(*del)(list->content, list->content_size);
		ft_memdel((void **)alist);
	}
}
