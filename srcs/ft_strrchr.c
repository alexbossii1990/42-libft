/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 06:09:08 by irabeson          #+#    #+#             */
/*   Updated: 2013/11/26 06:09:22 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *str, int c)
{
	const char		real_char = (char)c;
	char			*result;

	result = NULL;
	while (*str != '\0')
	{
		if (*str == real_char)
		{
			result = (char *)str;
		}
		str += 1;
	}
	if (*str == real_char)
		result = (char *)str;
	return (result);
}
