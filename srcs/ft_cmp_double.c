/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cmp_double.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/18 21:37:21 by irabeson          #+#    #+#             */
/*   Updated: 2014/05/18 21:37:24 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_numeric.h"
#include <float.h>

t_bool	ft_double_equ(double a, double b)
{
	return (ft_abs_f(a - b) < DBL_EPSILON);
}

t_bool	ft_double_nequ(double a, double b)
{
	return (ft_abs_f(a - b) > DBL_EPSILON);
}

t_bool	ft_double_gequ(double a, double b)
{
	return (ft_abs_f(a - b) < DBL_EPSILON || a > b);
}

t_bool	ft_double_lequ(double a, double b)
{
	return (ft_abs_f(a - b) < DBL_EPSILON || a < b);
}
