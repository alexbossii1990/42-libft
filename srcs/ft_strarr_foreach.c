/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strarr_foreach.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/24 14:50:02 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 06:58:24 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_strarr.h"
#include <stdlib.h>

void	strarr_foreach(char **array, t_apl_func afunc)
{
	const t_ui	count = strarr_size(array);
	t_ui		i;

	i = 0;
	if (array == NULL || *array == NULL)
		return ;
	while (i < count)
		afunc(array[i++]);
}
