/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strarr_append.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/24 14:27:55 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 06:58:56 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_strarr.h"
#include "ft_string.h"
#include <stdlib.h>

char	**strarr_append(char **array, char const *str)
{
	const t_ui	old_count = strarr_size(array);
	char		**new_array;

	new_array = strarr_realloc(array, old_count + 1);
	if (new_array == NULL)
		return (NULL);
	new_array[old_count] = ft_strdup(str);
	return (new_array);
}
