/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strbuf_access.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/24 18:04:46 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 06:27:35 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_strbuf.h"
#include "ft_string.h"

char	*strbuf_get(t_strbuf const *strbuf)
{
	if (strbuf == NULL)
		return (NULL);
	else
		return (ft_strndup(strbuf->buffer, strbuf->size));
}

char	*strbuf_getsub(t_strbuf const *strbuf, t_ui start, t_ui length)
{
	if (start >= strbuf->size)
		return (ft_strdup(""));
	if (start + length > strbuf->size)
		length = strbuf->size - start;
	return (ft_strndup(strbuf->buffer + start, length));
}

t_ui	strbuf_size(t_strbuf const *strbuf)
{
	return (strbuf->size);
}
