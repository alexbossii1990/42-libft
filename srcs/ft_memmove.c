/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 18:40:26 by irabeson          #+#    #+#             */
/*   Updated: 2013/11/27 12:41:17 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	*ft_memmove(void *s1, const void *s2, size_t n)
{
	size_t			i;
	unsigned char	*str1;
	unsigned char	*str2;
	unsigned char	*temp;

	i = 0;
	str1 = (unsigned char *)s1;
	str2 = (unsigned char *)s2;
	temp = (unsigned char *)malloc(sizeof(*temp) * n);
	if (temp)
	{
		while (i < n)
		{
			temp[i] = str2[i];
			i += 1;
		}
		i = 0;
		while (i < n)
		{
			str1[i] = temp[i];
			i += 1;
		}
		free(temp);
	}
	return ((void *)s1);
}
