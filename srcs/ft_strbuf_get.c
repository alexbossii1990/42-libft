/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strbuf_get.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/24 23:34:15 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 06:29:16 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_strbuf.h"

char	strbuf_get_char(t_strbuf const *strbuf, t_ui pos)
{
	return (strbuf->buffer[pos]);
}

char	strbuf_get_last_char(t_strbuf const *strbuf)
{
	return (strbuf->buffer[strbuf->size - 1]);
}
