/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstaddback.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson42@gmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/27 20:14:28 by irabeson          #+#    #+#             */
/*   Updated: 2013/11/28 02:01:34 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstaddback(t_list **alst, t_list *new_lst)
{
	t_list	*last;

	if (new_lst)
	{
		if (*alst)
		{
			last = *alst;
			while (last && last->next)
				last = last->next;
			if (last)
				last->next = new_lst;
		}
		else
			*alst = new_lst;
	}
}
