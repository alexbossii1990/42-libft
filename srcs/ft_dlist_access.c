/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlist_access.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/19 14:52:53 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/19 14:53:21 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_dlist.h"

t_dlist_node			*dlist_first(t_dlist *list)
{
	return (list->first);
}

t_dlist_node			*dlist_last(t_dlist *list)
{
	return (list->last);
}

t_dlist_node	const	*dlist_cfirst(t_dlist const *list)
{
	return (list->first);
}

t_dlist_node const		*dlist_clast(t_dlist const *list)
{
	return (list->last);
}
