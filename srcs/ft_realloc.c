/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/21 23:06:54 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 05:04:35 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_memory.h"
#include <stdlib.h>

void		*ft_realloc(void *ptr, size_t size_of, size_t new_size)
{
	unsigned char	*new;
	size_t			cpy_size;

	if (size_of == new_size)
		return (ptr);
	new = (unsigned char *)malloc(new_size);
	if (new && ptr)
	{
		cpy_size = (size_of < new_size) ? size_of : new_size;
		if (ptr && size_of > 0)
			ft_memcpy(new, ptr, cpy_size);
		if (ptr)
			free(ptr);
	}
	return (new);
}
