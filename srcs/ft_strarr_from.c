/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strarr_from.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/24 14:13:50 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 07:02:43 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_strarr.h"
#include "ft_dlist.h"
#include "ft_string.h"

char	**strarr_from_array(int count, char **strs)
{
	char	**new_array;
	int		i;

	new_array = strarr_malloc((t_ui)count);
	if (new_array == NULL)
		return (NULL);
	i = 0;
	while (i < count)
	{
		if (strs[i])
			new_array[i] = ft_strdup(strs[i]);
		++i;
	}
	new_array[i] = NULL;
	return (new_array);
}

char	**strarr_from_list(t_dlist const *str_list)
{
	t_ui const			count = dlist_count(str_list);
	char				**new_array;
	t_dlist_node const	*it;
	t_ui				i;

	new_array = strarr_malloc(count);
	if (new_array == NULL)
		return (NULL);
	it = dlist_cfirst(str_list);
	i = 0;
	while (it)
	{
		new_array[i] = ft_strdup((char *)it->item);
		++i;
		it = it->next;
	}
	new_array[count] = NULL;
	return (new_array);
}
