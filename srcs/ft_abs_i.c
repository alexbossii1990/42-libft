/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_abs_i.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/28 05:45:47 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 06:00:40 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_abs_i(int value)
{
	if (value < 0)
		return (-value);
	else
		return (value);
}
