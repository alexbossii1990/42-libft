/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlist_node.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/19 14:41:57 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/19 14:44:52 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_dlist_node.h"
#include <stdlib.h>

void	dlist_node_init(t_dlist_node *node, void *item)
{
	node->prev = NULL;
	node->next = NULL;
	node->item = item;
}

void	dlist_node_destroy(t_dlist_node *node, t_del_func dfunc)
{
	if (dfunc && node->item)
	{
		dfunc(node->item);
		node->item = NULL;
	}
	node->prev = NULL;
	node->next = NULL;
}

void	dlist_node_set_next(t_dlist_node *node, t_dlist_node *new_next)
{
	if (node == NULL)
		return ;
	if (node->next)
		node->next->prev = NULL;
	node->next = new_next;
	if (node->next)
		node->next->prev = node;
}

void	dlist_node_set_prev(t_dlist_node *node, t_dlist_node *new_prev)
{
	if (node == NULL)
		return ;
	if (node->prev)
		node->prev->next = NULL;
	node->prev = new_prev;
	if (node->prev)
		node->prev->next = node;
}
