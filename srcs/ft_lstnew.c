/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/27 14:20:38 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 05:12:19 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_memory.h"
#include <stdlib.h>

static void	*lst_alloc_content(void const *content,
								size_t content_size)
{
	void	*new_content;

	new_content = malloc(content_size);
	if (new_content)
		ft_memcpy(new_content, content, content_size);
	return (new_content);
}

t_list		*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*chain;

	chain = (t_list *)malloc(sizeof(*chain));
	if (chain == NULL)
		return (NULL);
	if (content == NULL)
	{
		chain->content = NULL;
		chain->content_size = 0;
	}
	else
	{
		chain->content = lst_alloc_content(content, content_size);
		chain->content_size = content_size;
	}
	chain->next = NULL;
	return (chain);
}
