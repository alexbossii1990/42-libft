/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strbuf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/24 17:54:26 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 06:44:12 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_strbuf.h"
#include <stdlib.h>

t_ui	strbuf_init(t_strbuf *strbuf, t_ui init_cap)
{
	strbuf->buffer = NULL;
	strbuf->size = 0;
	strbuf->capacity = 0;
	return (strbuf_reserve(strbuf, init_cap));
}

void	strbuf_destroy(t_strbuf *strbuf)
{
	if (strbuf == NULL)
		return ;
	if (strbuf->buffer)
	{
		free(strbuf->buffer);
		strbuf->buffer = NULL;
	}
	strbuf->size = 0;
	strbuf->capacity = 0;
}

t_ui	strbuf_reserve(t_strbuf *strbuf, t_ui new_cap)
{
	t_ui	i;
	t_ui	new_size;
	char	*new_buf;

	if (new_cap == strbuf->capacity)
		return (new_cap);
	new_buf = (char *)malloc(sizeof(*new_buf) * new_cap);
	if (new_buf == NULL)
		return (0);
	i = 0;
	new_size = (new_cap < strbuf->size) ? new_cap : strbuf->size;
	while (i < new_size)
	{
		new_buf[i] = strbuf->buffer[i];
		++i;
	}
	if (strbuf->buffer)
		free(strbuf->buffer);
	strbuf->buffer = new_buf;
	strbuf->size = new_size;
	strbuf->capacity = new_cap;
	return (new_cap);
}

void	strbuf_clear(t_strbuf *strbuf)
{
	strbuf->size = 0;
	if (strbuf->capacity > 0 && strbuf->buffer)
		strbuf->buffer[0] = '\0';
}
