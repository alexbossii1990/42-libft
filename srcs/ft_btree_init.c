/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_init.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/18 18:34:42 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/18 18:34:46 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"
#include <string.h>

void	btree_init(t_btree *tree, t_del_func dfunc,
					t_cmp_func cmp_func)
{
	tree->root = NULL;
	tree->dfunc = dfunc;
	tree->cmp_func = cmp_func;
}
