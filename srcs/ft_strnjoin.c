/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/21 20:41:48 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 05:10:58 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"
#include <stdlib.h>

char	*ft_strnjoin(char const *str1, char const *str2, size_t n)
{
	char	*new_str;
	char	*result;
	size_t	str2_len;

	if (str1 == NULL || str2 == NULL)
		return (NULL);
	str2_len = ft_strlen(str2);
	n = (n > str2_len) ? str2_len : n;
	new_str = (char *)malloc(sizeof(*new_str) * (ft_strlen(str1)
				+ n + 1));
	result = NULL;
	if (new_str)
	{
		result = new_str;
		ft_strcpy(new_str, str1);
		new_str += ft_strlen(str1);
		ft_strncpy(new_str, str2, n);
		new_str += n;
		*new_str = '\0';
	}
	return (result);
}
