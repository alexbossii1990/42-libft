/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strbuf_clone.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/09 15:38:44 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 06:45:38 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_strbuf.h"

void	strbuf_clone(t_strbuf *clone, t_strbuf const *strbuf)
{
	strbuf_clear(clone);
	strbuf_app_strn(clone, strbuf->buffer, strbuf->size);
}
