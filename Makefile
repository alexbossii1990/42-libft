# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/11/19 10:49:54 by irabeson          #+#    #+#              #
#*   Updated: 2014/06/20 04:51:38 by irabeson         ###   ########.fr       *#
#                                                                              #
# **************************************************************************** #

NAME = libft
CC = gcc
CFLAGS = -Wall -Werror -Wextra $(CMODE) 
INCLUDES = -I./includes/
SRC_DIR = ./srcs
BUILD_DIR = ./builds
OBJS = $(addprefix $(BUILD_DIR)/, $(SRC:.c=.o))
MODE = release
# debug or release mode
ifeq ($(MODE), debug)
	CMODE = -g
	CL_ID = \033[0;32m
else
	CMODE = -O3
	CL_ID = \033[0;34m
endif
CL_NO = \033[0m
CL_RM = \033[0;31m
TARGET = $(NAME).a
SRC = 	$(SRC_BTREE)		\
		$(SRC_DLIST)		\
		$(SRC_GETFLAG)		\
		$(SRC_GETOPT)		\
		$(SRC_PRINT)		\
		$(SRC_STRING)		\
		$(SRC_STRBUF)		\
		$(SRC_STRARR)		\
		$(SRC_MINMAX)		\
		$(SRC_MEMORY)		\
		$(SRC_NUMERIC)		\
		ft_lstnew.c			\
		ft_lstdelone.c		\
		ft_lstdel.c			\
		ft_lstadd.c			\
		ft_lstsize.c		\
		ft_lstiter.c		\
		ft_lstmap.c			\
		ft_lstaddback.c		\

SRC_STRING =						\
		ft_strcmp.c					\
		ft_strncmp.c				\
		ft_strlen.c					\
		ft_strdup.c					\
		ft_strndup.c				\
		ft_strcpy.c					\
		ft_strncpy.c				\
		ft_strcat.c					\
		ft_strncat.c				\
		ft_strlcat.c				\
		ft_strchr.c					\
		ft_strnchr.c				\
		ft_strrchr.c				\
		ft_strstr.c					\
		ft_strnstr.c				\
		ft_isalpha.c				\
		ft_isupper.c				\
		ft_islower.c				\
		ft_isdigit.c				\
		ft_isalnum.c				\
		ft_isascii.c				\
		ft_isprint.c				\
		ft_tolower.c				\
		ft_toupper.c				\
		ft_strnew.c					\
		ft_strdel.c					\
		ft_strclr.c					\
		ft_striter.c				\
		ft_striteri.c				\
		ft_strmap.c					\
		ft_strmapi.c				\
		ft_strsub.c					\
		ft_strjoin.c				\
		ft_strnjoin.c				\
		ft_strtrim.c				\
		ft_strsplit.c				\
		ft_strsplits.c				\
		ft_strsplit_dlist.c			\
		ft_strsplits_dlist.c		\
		ft_strequ.c					\
		ft_strnequ.c				\
		ft_itoa.c					\
		ft_uitoa.c					\
		ft_lltoa.c					\
		ft_ulltoa.c					\
		ft_ltoa.c					\
		ft_ultoa.c					\
		ft_atoi.c					\
		ft_match.c					\
		ft_strfind.c				\
		ft_strrep.c					\
		ft_strnrep.c				\
		ft_gnl.c

SRC_STRARR =						\
		ft_strarr.c					\
		ft_strarr_from.c			\
		ft_strarr_foreach.c			\
		ft_strarr_append.c			\
		ft_strarr_find.c

SRC_STRBUF =						\
		ft_strbuf.c					\
		ft_strbuf_insert.c			\
		ft_strbuf_replace.c			\
		ft_strbuf_pop.c				\
		ft_strbuf_erase.c			\
		ft_strbuf_access.c			\
		ft_strbuf_app.c				\
		ft_strbuf_get.c				\
		ft_strbuf_clone.c			\
		ft_strbuf_find.c

SRC_NUMERIC =						\
		ft_abs_i.c					\
		ft_abs_l.c					\
		ft_abs_ll.c					\
		ft_abs_f.c					\
		ft_abs_d.c					\
		ft_cmp_float.c				\
		ft_cmp_double.c				\
		ft_minmax_int.c				\
		ft_minmax_uint.c			\
		ft_minmax_long.c			\
		ft_minmax_ulong.c			\
		ft_minmax_llong.c			\
		ft_minmax_ullong.c			\
		ft_minmax_size_t.c			\
		ft_minmax_float.c			\
		ft_minmax_double.c

SRC_PRINT =							\
		ft_putchar.c				\
		ft_putchar_fd.c				\
		ft_putstr.c					\
		ft_putstr_fd.c				\
		ft_putstrn.c				\
		ft_putstrn_fd.c				\
		ft_putendl.c				\
		ft_putendl_fd.c				\
		ft_putl.c					\
		ft_putl_fd.c				\
		ft_putll.c					\
		ft_putll_fd.c				\
		ft_putnbr.c					\
		ft_putnbr_fd.c				\
		ft_putui.c					\
		ft_putui_fd.c				\
		ft_putul.c					\
		ft_putul_fd.c				\
		ft_putull.c					\
		ft_putull_fd.c

SRC_MEMORY = 						\
		ft_memset.c					\
		ft_bzero.c					\
		ft_memcpy.c					\
		ft_memccpy.c				\
		ft_memmove.c				\
		ft_realloc.c				\
		ft_memchr.c					\
		ft_memcmp.c					\
		ft_memalloc.c				\
		ft_memdel.c

SRC_BTREE =							\
		ft_btree_contains.c			\
		ft_btree_copy.c				\
		ft_btree_clear.c			\
		ft_btree_destroy.c			\
		ft_btree_empty.c			\
		ft_btree_erase.c			\
		ft_btree_find.c				\
		ft_btree_init.c				\
		ft_btree_insert.c			\
		ft_btree_iter.c				\
		ft_btree_minmax.c			\
		ft_btree_node_access.c		\
		ft_btree_node.c				\
		ft_btree_node_iter.c		\
		ft_btree_node_query.c		\
		ft_btree_remove.c			\
		ft_btree_size.c				\
		ft_btree_transfer_if.c

SRC_DLIST = 						\
		ft_dlist_node.c				\
		ft_dlist.c					\
		ft_dlist_access.c			\
		ft_dlist_push_pop.c			\
		ft_dlist_remove.c			\
		ft_dlist_query.c			\
		ft_dlist_foreach.c

SRC_GETFLAG =						\
		ft_getflag.c				\
		ft_getflag_parse.c

SRC_GETOPT =						\
		ft_getopt.c					\
		ft_getopt_access.c			\
		ft_getopt_parse.c			\
		ft_go_flag_init.c			\
		ft_go_flag_destroy.c		\
		ft_go_init_functions2.c		\
		ft_go_init_functions.c

.PHONY: all clean fclean re

default: all

all: $(NAME)
	@echo $(NAME) "is ready."

$(NAME): $(BUILD_DIR) $(TARGET)

$(TARGET): $(OBJS)
	@echo " + Archiving $(CL_ID)$@$(CL_NO)"
	@ar -rs $(TARGET) $(OBJS)

clean:
	@echo " - removing object files"
	@rm -f $(OBJS)

fclean: clean
	@echo " - removing $(CL_RM)$(TARGET)$(CL_NO)"
	@rm -f $(TARGET)

re: fclean $(NAME)

$(addprefix $(BUILD_DIR)/, %.o): $(addprefix $(SRC_DIR)/, %.c)
	@echo " + Compiling $(CL_ID)$^$(CL_NO)"
	@$(CC) $(INCLUDES) $(CFLAGS) -o $@ -c $<

$(BUILD_DIR):
	@echo " + Creating $(COL_ID)directory$(COL_NO)"
	@mkdir -p $(BUILD_DIR)
