/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlist.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/19 14:40:02 by irabeson          #+#    #+#             */
/*   Updated: 2014/06/01 07:15:30 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_DLIST_H
# define FT_DLIST_H
# include "ft_dlist_node.h"
# include "ft_types_def.h"

typedef struct		s_dlist
{
	t_del_func		dfunc;
	t_dlist_node	*first;
	t_dlist_node	*last;
}					t_dlist;

void				dlist_init(t_dlist *list, t_del_func dfunc);
void				dlist_destroy(t_dlist *list);
t_ui				dlist_count(t_dlist const *list);
t_bool				dlist_empty(t_dlist const *list);
t_dlist_node		*dlist_push_front(t_dlist *list, void *item);
t_dlist_node		*dlist_push_back(t_dlist *list, void *item);
void				dlist_pop_front(t_dlist *list);
void				dlist_pop_back(t_dlist *list);
t_dlist_node		*dlist_insert(t_dlist *list, t_dlist_node *at, void *item);
t_dlist_node		*dlist_erase(t_dlist *list, t_dlist_node *node);
t_dlist_node		*dlist_remove(t_dlist *list, t_dlist_node *node);
void				dlist_clear(t_dlist *list);
t_dlist_node		*dlist_first(t_dlist *list);
t_dlist_node		*dlist_last(t_dlist *list);
t_dlist_node const	*dlist_cfirst(t_dlist const *list);
t_dlist_node const	*dlist_clast(t_dlist const *list);
void				dlist_foreach(t_dlist *list, t_apl_func afunc);
void				dlist_cforeach(t_dlist const *list, t_apl_func afunc);
t_ui				dlist_erase_if(t_dlist *list, t_filter_func ffunc);

#endif
