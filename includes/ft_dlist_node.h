/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlist_node.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/19 14:40:37 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/25 19:09:10 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_DLIST_NODE_H
# define FT_DLIST_NODE_H
# include "ft_functional.h"

typedef struct	s_dlist_node
{
	struct s_dlist_node	*prev;
	struct s_dlist_node	*next;
	void				*item;
}				t_dlist_node;

void			dlist_node_init(t_dlist_node *node, void *item);
void			dlist_node_destroy(t_dlist_node *node, t_del_func dfunc);
void			dlist_node_set_next(t_dlist_node *node,
									t_dlist_node *new_next);
void			dlist_node_set_prev(t_dlist_node *node,
									t_dlist_node *new_prev);

#endif
