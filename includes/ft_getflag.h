/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getflag.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/16 16:30:36 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/25 01:35:17 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_GETFLAG_H
# define FT_GETFLAG_H
# include "ft_types_def.h"

struct s_getflag;

typedef enum	e_getflag_err
{
	INVALID_FLAG = 0,
	REPEATED_FLAG
}				t_getflag_err;

typedef void(*t_getflag_err_cb)(struct s_getflag *gf, t_getflag_err type,
								char flag_char);

typedef struct	s_getflag_pair
{
	char	char_value;
	t_bool	enabled;
}				t_getflag_pair;

typedef struct	s_getflag
{
	t_getflag_pair		*fgpairs;
	char				*available_flags;
	t_ui				count;
	t_getflag_err_cb	error_cb;
}				t_getflag;

void			getflag_init(t_getflag *getflag,
								char const *available_chars,
								t_getflag_err_cb cb);
void			getflag_destroy(t_getflag *getflag);
t_bool			getflag_enable(t_getflag *getflag, char c);
void			getflag_loads(t_getflag *getflag, char const *arg);
t_bool			getflag_is_enabled(t_getflag const *getflag, char c);
int				getflag_parse_args(t_getflag *getflag, int argc, char **argv);
char const		*getflag_available_flags(t_getflag const *getflag);

#endif
