/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strarr.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/22 05:39:04 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/28 07:04:54 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STRARR_H
# define FT_STRARR_H
# include "ft_types_def.h"
# include "ft_functional.h"

struct s_dlist;

char	**strarr_malloc(t_ui size);
char	**strarr_realloc(char **array, t_ui new_size);
char	**strarr_from_array(int count, char **strs);
char	**strarr_from_list(struct s_dlist const *str_list);
void	strarr_free(char **array);
char	**strarr_append(char **array, char const *str);
void	strarr_foreach(char **array, t_apl_func afunc);
void	strarr_sort(char **array, t_cmp_func cmp_func);
t_ui	strarr_size(char **array);
t_bool	strarr_find(char **array, char const *str, t_ui *index);

#endif
