/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strbuf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/24 17:47:25 by irabeson          #+#    #+#             */
/*   Updated: 2014/06/20 02:26:51 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STRBUF_H
# define FT_STRBUF_H
# include "ft_types_def.h"

typedef struct	s_strbuf
{
	t_ui	size;
	t_ui	capacity;
	char	*buffer;
}				t_strbuf;

t_ui			strbuf_init(t_strbuf *strbuf, t_ui init_cap);
void			strbuf_destroy(t_strbuf *strbuf);
void			strbuf_clone(t_strbuf *clone, t_strbuf const *strbuf);
t_ui			strbuf_reserve(t_strbuf *strbuf, t_ui new_cap);
void			strbuf_clear(t_strbuf *strbuf);
char			*strbuf_get(t_strbuf const *strbuf);
char			*strbuf_getsub(t_strbuf const *strbuf, t_ui start, t_ui length);
char			strbuf_get_char(t_strbuf const *strbuf, t_ui pos);
char			strbuf_get_last_char(t_strbuf const *strbuf);
t_ui			strbuf_size(t_strbuf const *strbuf);
void			strbuf_app_char(t_strbuf *strbuf, char c);
void			strbuf_app_str(t_strbuf *strbuf, char const *str);
void			strbuf_app_strn(t_strbuf *strbuf, char const *str, t_ui n);
void			strbuf_insert_char(t_strbuf *strbuf, t_ui pos, char c);
void			strbuf_insert_str(t_strbuf *strbuf, t_ui pos, char const *str);
void			strbuf_insert_strn(t_strbuf *strbuf, t_ui pos, char const *str,
									t_ui n);
t_ui			strbuf_erase(t_strbuf *strbuf, t_ui start, t_ui length);
void			strbuf_pop_front(t_strbuf *strbuf);
void			strbuf_pop_back(t_strbuf *strbuf);
t_bool			strbuf_find_char(t_strbuf const *str, char c, t_ui *pos);
t_bool			strbuf_find(t_strbuf *strbuf, char const *search, t_ui *start);
void			strbuf_replace_all(t_strbuf *strbuf, char const *rep_str,
									char const *new_str);

#endif
