/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_numeric.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/28 06:00:15 by irabeson          #+#    #+#             */
/*   Updated: 2014/05/13 00:59:48 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_NUMERIC_H
# define FT_NUMERIC_H
# include "ft_types_def.h"
# include <stddef.h>
# define FT_PI_F	3.141593f
# define FT_PI_D2_F	1.570796f

int					ft_min_int(int a, int b);
long				ft_min_long(long a, long b);
long long			ft_min_llong(long long a, long long b);
unsigned int		ft_min_uint(unsigned int a, unsigned int b);
unsigned int long	ft_min_ulong(unsigned long a, unsigned long b);
unsigned long long	ft_min_ullong(unsigned long long a, unsigned long long b);
size_t				ft_min_size_t(size_t a, size_t b);
float				ft_min_float(float a, float b);
float				ft_min_double(double a, double b);
int					ft_max_int(int a, int b);
long				ft_max_long(long a, long b);
long long			ft_max_llong(long long a, long long b);
unsigned int		ft_max_uint(unsigned int a, unsigned int b);
unsigned int long	ft_max_ulong(unsigned long a, unsigned long b);
unsigned long long	ft_max_ullong(unsigned long long a, unsigned long long b);
size_t				ft_max_size_t(size_t a, size_t b);
float				ft_max_float(float a, float b);
float				ft_max_double(double a, double b);
float				ft_abs_f(float value);
double				ft_abs_d(double value);
int					ft_abs_i(int value);
long				ft_abs_l(long value);
long long			ft_abs_ll(long long value);
t_bool				ft_float_equ(float a, float b);
t_bool				ft_float_nequ(float a, float b);
t_bool				ft_float_gequ(float a, float b);
t_bool				ft_float_lequ(float a, float b);
t_bool				ft_double_equ(double a, double b);
t_bool				ft_double_nequ(double a, double b);
t_bool				ft_double_gequ(double a, double b);
t_bool				ft_double_lequ(double a, double b);
#endif
