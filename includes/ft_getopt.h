/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getopt.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@tudent.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/12 01:51:12 by irabeson          #+#    #+#             */
/*   Updated: 2014/04/25 18:33:53 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_GETOPT_H
# define FT_GETOPT_H

# include <ft_getopt_flag.h>
# include <ft_btree.h>

typedef struct	s_getopt
{
	t_btree	flags;
}				t_getopt;

void			ft_getopt_init_args(t_getopt *getopt, int argc, char **argv);
void			ft_getopt_init(t_getopt *getopt);
void			ft_getopt_destroy(t_getopt *getopt);
int				ft_getopt_contains(t_getopt const *getopt, char const *flag);
int				ft_getopt_hasvalue(t_getopt const *getopt, char const *flag);
char			*ft_getopt_value(t_getopt const *getopt, char const *flag);
t_go_flag		*ft_getopt_parse(t_getopt *getopt, char const *str, size_t i);

#endif
