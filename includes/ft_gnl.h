/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gnl.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: irabeson <irabeson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/20 01:12:20 by irabeson          #+#    #+#             */
/*   Updated: 2015/01/23 17:00:25 by irabeson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_GNL_H
# define FT_GNL_H
# include <ft_strbuf.h>

# define GNL_BUFFER_SIZE	16

typedef enum	e_gnl_state
{
	GNL_END = 0,
	GNL_CONTINUE = 1,
	GNL_ERROR = -1
}				t_gnl_state;

typedef struct	s_gnl
{
	t_strbuf	buffer;
}				t_gnl;

void			gnl_init(t_gnl *gnl);
void			gnl_destroy(t_gnl *gnl);
int				gnl_readline(t_gnl *gnl, int fd, char **line);

#endif
